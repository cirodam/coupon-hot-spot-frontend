import React from 'react';

import Layout from '../components/Layout';
import globals from '../globals.css';

import { Provider } from 'react-redux';
import {useStore} from '../redux/store';

const MyApp = ({Component, pageProps}) => {

    const store = useStore(pageProps.initialReduxState)

    return (
        <Provider store={store}>
            <Layout>
                <Component {...pageProps}/>
            </Layout>
        </Provider>
    )
}

export default MyApp;
