import React from 'react';
import { useSelector } from 'react-redux';
import styles from './latest.module.css';
import SearchItem from '../components/SearchItem';
import useRefreshCategories from '../hooks/useRefreshCategories';
import useRefreshShops from '../hooks/useRefreshShops';
import useRefreshDeals from '../hooks/useRefreshDeals';

const latest = () => {

  const {latest} = useSelector(state => state.deals);

  useRefreshCategories();
  useRefreshShops();
  useRefreshDeals();

  return (
    <div className={styles.latestPage}>
        <h1 className={styles.pageHeader}>Latest Deals</h1>
        <ul className={styles.couponList}>
            {
                latest && latest.map(l => (
                    <SearchItem coupon={l}/>
                ))
            }
        </ul>
    </div>
  )
}

export default latest