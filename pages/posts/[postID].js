import React from 'react';
import axios from 'axios';
import parse from 'react-html-parser';
import styles from './[postID].module.css';
import Link from 'next/link';

export async function getServerSideProps({query}) {

    const id = +query.postID || -1;

    try {

        let post = await axios.get(`http://54.160.183.136/wp-json/wp/v2/posts?include[]=${id}`);
        return {
            props: {post: post.data[0], id}, 
          }

    } catch (error) {

        return {
            props: {error: JSON.stringify(error), id}, 
          }
    }
}

const postID = ({post, error, id}) => {

    console.log(post);
    console.log(error);
    console.log(id);

    return (
        <div id={styles.postPage}>
            <div id={styles.container}>
                <div id={styles.postTop}>
                    <img src={post.jetpack_featured_media_url} alt="Icon" id={styles.postImg} />
                    <Link href="/posts/"><i id={styles.backBtn} className="fas fa-arrow-left"></i></Link>
                    <div id={styles.postRight}>
                        <h1 id={styles.postHeader}>{post.title.rendered}</h1>
                        <p id={styles.postDate}>{post.date.substr(0,10)}</p>
                        <p id={styles.postMeta}>5 Minute Read</p>
                    </div>
                </div>
                <div id={styles.postContent}>
                    {
                        parse(post.content.rendered)
                    }
                </div>
            </div>
        </div>
    )
}

export default postID; 
