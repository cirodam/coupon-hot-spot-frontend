import React, {useState} from 'react';
import styles from './contact.module.css';

const contact = () => {

    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        message: ""
    });
    const {firstName, lastName, email, message} = formData;
    const [formSubmitted, setFormSubmitted] = useState(false);

    const onInputChange = e => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    const onSubmitBtn = async e => {
        e.preventDefault();

        const body = {firstName, lastName, email, message};

        try {
        //   await axios.post('https://29oddee2eh.execute-api.us-east-1.amazonaws.com/dev/contact', body);
        //   setFormData({
        //     firstName: "",
        //     lastName: "",
        //     email: "",
        //     message: ""
        //   })
          setFormSubmitted(true);

        } catch (err) {
          
        }
    }

    return (
        <div id={styles.contactPage}>
            <h1 id={styles.pageHeader}>Contact Us</h1>
            <form id={styles.contactForm}>
                <input type="text" name="firstName" id={styles.firstNameInput} placeholder="First Name" className="input" value={firstName} onChange={e => onInputChange(e)} />
                <input type="text" name="lastName" id={styles.lastNameInput} placeholder="Last Name" className="input" value={lastName} onChange={e => onInputChange(e)} />
                <input type="email" name="email" id={styles.emailInput} placeholder="Email Address" className="input" value={email} onChange={e => onInputChange(e)} />
                <textarea name="message" id={styles.messageTextArea} placeholder="Message" className="input" value={message} onChange={e => onInputChange(e)} ></textarea>
                {formSubmitted && <p id={styles.submitText}>Form Submitted!</p>}
            </form>
            <button onClick={(e) => onSubmitBtn(e)} id={styles.submitBtn}>Contact Me</button>
        </div>
    )
}

export default contact
