import React from 'react';
import { useSelector } from 'react-redux';
import Head from 'next/head';

import styles from './shops.module.css';
import ShopItem from '../components/shops/ShopItem'
import useRefreshShops from '../hooks/useRefreshShops';

const shops = () => {

    useRefreshShops();
    const {shops} = useSelector(state => state.shops);

    return (
        <>
            <Head>
                <title>Coupon Hotspot | Stores</title>
            </Head>
            <div id={styles.shopsPage}>
                <div id={styles.shopsContainer}>
                    <h1 id={styles.shopsHeader}>Our Partners</h1>
                    <ul id={styles.shopsList}>
                        { shops &&
                            shops.map(s => (
                                <ShopItem key={s.shopName} shop={s}/>
                            ))
                        }
                    </ul>
                </div>
            </div>
        </>
    )
}

export default shops
