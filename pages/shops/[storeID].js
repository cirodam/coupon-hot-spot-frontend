import React from 'react';
import axios from 'axios';
import { getOptions } from '../../util';
import styles from './[storeID].module.css';
import SearchItem from '../../components/SearchItem';

export async function getServerSideProps(context) {

  let store = {};
  let coupons = [];

  //Get Store
  try {
      let res = await axios.get(`https://ujmadp5wr9.execute-api.us-east-1.amazonaws.com/dev/stores/${context.params.storeID}`, getOptions);
      store = res.data;

  } catch (error) {
      return { props: {error: JSON.stringify("Error")} }
  }

  //Get Coupons
  try {
    let res = await axios.post(`https://ujmadp5wr9.execute-api.us-east-1.amazonaws.com/dev/query?count=20`, {term: store.storeName, fields: ["shopName"]}, getOptions);
    coupons = res.data.coupons;

  } catch (error) {
      return { props: {error: JSON.stringify("Error")} }
  }

  return {
    props: {store, coupons} 
  }

}

const Store = ({store, coupons}) => {

  console.log(store);
  console.log(coupons);

  return (
    <div className={styles.storePage}>
      <h1 className={styles.pageHeading}>{store && store.storeName} | <span className={styles.dealText}>{store && store.couponCount} Deals</span></h1>
      <ul className={styles.couponList}>
      {
        coupons.map(c => (
          <SearchItem coupon={c}/>
        ))
      }
      </ul>
    </div>
  )
}

export default Store;