import React from 'react';
import styles from './[catName].module.css';
import axios from 'axios';
import { getOptions } from '../../util';
import SearchItem from '../../components/SearchItem';

export async function getServerSideProps(context) {

    let coupons = [];
  
    //Get Coupons
    try {
      let res = await axios.post(`https://ujmadp5wr9.execute-api.us-east-1.amazonaws.com/dev/query`, {term: context.params.catName, fields: ["categoryName"]}, getOptions);
      coupons = res.data.coupons;
  
    } catch (error) {
        return { props: {error: JSON.stringify("Error")} }
    }
  
    return {
      props: {coupons, categoryName: context.params.catName} 
    }
  
  }

const Category = ({coupons, categoryName}) => {

  return (
    <div className={styles.latestPage}>
    <h1 className={styles.pageHeader}>{categoryName}</h1>
    <ul className={styles.couponList}>
        {
            coupons && coupons.map(l => (
                <SearchItem coupon={l}/>
            ))
        }
    </ul>
    </div>
  )
}

export default Category