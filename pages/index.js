import React from 'react';
import CarouselSection from '../components/carousel/CarouselSection';
import HeroSection from '../components/hero/HeroSection';
import HowSection from '../components/how/HowSection';
import LatestSection from '../components/latest/LatestSection';
import ShopsSection from '../components/shops/ShopsSection';
import Head from 'next/head';
import useRefreshCategories from '../hooks/useRefreshCategories';
import useRefreshShops from '../hooks/useRefreshShops';
import useRefreshDeals from '../hooks/useRefreshDeals';
import styles from './index.module.css';

const index = () => {

    useRefreshCategories();
    useRefreshShops();
    useRefreshDeals();

    return (
        <>
            <Head>
                <title>Coupon Hotspot | On The Spot Deals</title>
            </Head>
            <div id={styles.index}>
                <HeroSection />
                <CarouselSection />
                <LatestSection />
                <ShopsSection />
                <HowSection />
            </div>
        </>
    )
}

export default index;
