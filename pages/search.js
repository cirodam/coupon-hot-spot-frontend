import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './search.module.css';
import {searchFor} from '../redux/actions/searchActions';
import SearchItem from '../components/SearchItem';
import Head from 'next/head';
import { useRouter } from 'next/dist/client/router';

export async function getServerSideProps({query}) {

    const category = query.category || "";
    const term = query.term || "";
    const shop = query.shop || "";
    const page = query.page || 0;

    return {
      props: {category, term, shop, page}, 
    }
}

const search = ({category, term, shop, page}) => {

    const dispatch = useDispatch()
    const router = useRouter();
    const {results, pages} = useSelector(state => state.search);

    useEffect(() => {
        dispatch(searchFor(category, term, shop, page*20, 20));

    }, [term, category, shop, page])

    let buttons = [];

    if(pages){
        for(let i=0; i<pages; i++){
            buttons.push((<button className={`${styles.navBtn} ${i === +page && styles.current}`} onClick={() => onPageBtn(i)}>{i + 1}</button>))
        }
    }

    const onPageBtn = (p) => {
        router.push(`/search?category=${category}&term=${term}&shop=${shop}&page=${p}`);
    }

    const onNextBtn = () => {
        router.push(`/search?category=${category}&term=${term}&shop=${shop}&page=${(+page)+1}`);
    }

    const onPrevBtn = () => {
        router.push(`/search?category=${category}&term=${term}&shop=${shop}&page=${(+page)-1}`);
    }

    return (
        <>
            <Head>
                <title>Coupon Hotspot | Search Results</title>
            </Head>
            <div id={styles.searchPage}>
                <div id={styles.container}>
                    <h1 id={styles.searchHeader}>Search Results</h1>
                    <ul id={styles.searchList}>
                        { results &&
                            results.map(r => (
                                <SearchItem key={r.addTime} coupon={r}/>
                            ))
                        }
                    </ul>
                    <div>
                        <button className={styles.navBtn} onClick={() => onPrevBtn()}><i className="fas fa-chevron-left"></i></button>
                        {
                            buttons
                        }
                        <button className={styles.navBtn} onClick={() => onNextBtn()}><i className="fas fa-chevron-right"></i></button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default search
