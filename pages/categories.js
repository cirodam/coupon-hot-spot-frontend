import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './categories.module.css';
import { getCategories } from '../redux/actions/categoryActions';
import { useRouter } from 'next/router';
import CategoryItem from '../components/CategoryItem';
import Head from 'next/head';

const categories = () => {

    const dispatch = useDispatch();
    const {categories} = useSelector(state => state.categories);
    const router = useRouter()

    useEffect(() => {
        if(!categories){
            dispatch(getCategories())
        }
    })

    return (
        <>
            <Head>
                <title>Coupon Hotspot | Categories</title>
            </Head>
            <div id={styles.shopsPage}>
                <div id={styles.shopsContainer}>
                    <h1 id={styles.shopsHeader}>Categories</h1>
                    <ul id={styles.shopsList}>
                        { categories &&
                            categories.map(s => (
                                <CategoryItem key={s.categoryName} category={s}/>
                            ))
                        }
                    </ul>
                </div>
            </div>
        </>
    )
}

export default categories
