import React from 'react';
import styles from './techdeals.module.css';
import axios from 'axios';
import { getOptions } from '../util';
import SearchItem from '../components/SearchItem';

export async function getServerSideProps() {

  let coupons = [];

  //Get Store
  try {
      let res = await axios.post(`https://ujmadp5wr9.execute-api.us-east-1.amazonaws.com/dev/query`, {term: "techdeal", fields: ["group"]}, getOptions);
      coupons = res.data.coupons;

  } catch (error) {
      return { props: {error: JSON.stringify("Error")} }
  }

  return {
    props: {coupons} 
  }
}

const techdeals = ({coupons}) => {
  return (
    <div className={styles.techPage}>
        <h1 className={styles.pageHeader}>TECH DEALS OF THE DAY</h1>
        <ul className={styles.couponList}>
        {
          coupons && coupons.map(c => (
            <SearchItem coupon={c}/>
          ))
        }
        </ul>
    </div>
  )
}

export default techdeals