import axios from 'axios';
import { API_ROOT } from '../../const';
import { getOptions } from '../../util';

export const SEARCH_FINISHED = "SEARCH_FINISHED";

export const searchFor = (category, term, shop, start, count) => async dispatch => {

    let body = {
        term,
        fields: ["couponName", "couponDescription"],
        start,
        count
    }

    try {
        let res = await axios.post(API_ROOT + "/query", body, getOptions())
        console.log(res);
        dispatch({type: SEARCH_FINISHED, payload: {coupons: res.data.coupons, pages: res.data.pages}});

    } catch (err) {
        console.log("error" + err);
    }
}
