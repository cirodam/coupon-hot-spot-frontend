import axios from "axios";
import { API_ROOT } from "../../const";
import {getOptions} from '../../util';

export const CATEGORIES_LOADING = "CATEGORIES_LOADING";
export const CATEGORIES_LOADED = "CATEGORIES_LOADED";

export const getCategories = () => async dispatch => {

    try {
        let res = await axios.get(API_ROOT + "/categories", getOptions());
        dispatch({type: CATEGORIES_LOADED, payload: res.data});

    } catch (err) {
        console.log("error" + err);

    }
}
