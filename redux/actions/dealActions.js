import axios from "axios";
import { API_ROOT } from "../../const";
import {getOptions} from '../../util';

export const LATEST_LOADED = "LATEST_LOADED";
export const FEATURED_LOADED = "FEATURED_LOADED";
export const POPULAR_LOADED = "POPULAR_LOADED";
export const SUGGESTED_LOADED = "SUGGESTED_LOADED";

export const getLatest = () => async dispatch => {

    try {
        let res = await axios.get(API_ROOT + `/coupons/latest?count=100`, getOptions());
        dispatch({type: LATEST_LOADED, payload: res.data});

    } catch (err) {
        console.log("error" + err);
    }
}

export const getFeatured = () => async dispatch => {

    try {
        let res = await axios.post(API_ROOT + `/query`, {term: "featured", fields: ["group"]}, getOptions());
        dispatch({type: FEATURED_LOADED, payload: res.data});

    } catch (err) {
        console.log("error" + err);
    }
}

export const getPopular = () => async dispatch => {

    try {
        let res = await axios.post(API_ROOT + `/query`, {term: "popular", fields: ["group"]}, getOptions());
        dispatch({type: POPULAR_LOADED, payload: res.data});

    } catch (err) {
        console.log("error" + err);
    }
}

export const getSuggested = () => async dispatch => {

    try {
        let res = await axios.post(API_ROOT + `/query`, {term: "suggested", fields: ["group"]}, getOptions());
        dispatch({type: SUGGESTED_LOADED, payload: res.data});

    } catch (err) {
        console.log("error" + err);
    }
}
