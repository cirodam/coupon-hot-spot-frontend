import axios from 'axios';
import { API_ROOT } from '../../const';
import { getOptions } from '../../util';

export const SHOPS_LOADED = "SHOPS_LOADED";

export const getShops = () => async dispatch => {

    try {
        let res = await axios.get(API_ROOT + "/shops", getOptions());
        dispatch({type: SHOPS_LOADED, payload: res.data});

    } catch (err) {
        console.log("error" + err);
    }
}
