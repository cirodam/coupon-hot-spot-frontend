import { CATEGORIES_LOADED, CATEGORIES_LOADING } from "../actions/categoryActions";

const initialState = {
    categoriesLoading: false,
    categories: null
}

const categoryReducer = (state = initialState, {type, payload}) => {

    switch(type) {

        case CATEGORIES_LOADING:
            return {
                ...state, 
                categoriesLoading: true
            }

        case CATEGORIES_LOADED:
            return {
                ...state,
                categories: payload,
                categoriesLoading: false
            }
            
        default:
            return {...state}
    }
};

export default categoryReducer;