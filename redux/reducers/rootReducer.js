import dealReducer from './dealReducer';
import {combineReducers} from 'redux';
import shopReducer from './shopReducer';
import categoryReducer from './categoryReducer';
import searchReducer from './searchReducer';

const rootReducer = combineReducers({
    deals: dealReducer,
    shops: shopReducer,
    categories: categoryReducer,
    search: searchReducer
});

export default rootReducer;