import { SHOPS_LOADED } from "../actions/shopActions";

const initialState = {
    shops: null
}

const shopReducer = (state = initialState, {type, payload}) => {

    switch(type) {
        case SHOPS_LOADED:
            return {
                ...state,
                shops: payload
            }
            
        default:
            return {...state}
    }
};

export default shopReducer;