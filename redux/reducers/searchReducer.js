import { SEARCH_FINISHED } from "../actions/searchActions";

const initialState = {
    results: null,
    pages: 0
}

const searchReducer = (state = initialState, {type, payload}) => {

    switch(type) {
        case SEARCH_FINISHED:
            return {
                ...state,
                results: payload.coupons,
                pages: payload.pages
            }
            
        default:
            return {...state}
    }
};

export default searchReducer;