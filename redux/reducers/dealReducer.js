import { FEATURED_LOADED, LATEST_LOADED, POPULAR_LOADED, SUGGESTED_LOADED } from "../actions/dealActions";

const initialState = {
    latest: null,
    featured: null,
    popular: null,
    suggested: null,
    dealsLoading: false
}

const dealReducer = (state = initialState, {type, payload}) => {

    switch(type) {
        case LATEST_LOADED:
            return {
                ...state,
                latest: payload
            }

        case FEATURED_LOADED:
            return {
                ...state,
                featured: payload.coupons,
            }
    
        case POPULAR_LOADED:
            return {
                ...state,
                popular: payload.coupons
            }

        case SUGGESTED_LOADED:
            return {
                ...state,
                suggested: payload.coupons
            }
            
        default:
            return {...state}
    }
};

export default dealReducer;