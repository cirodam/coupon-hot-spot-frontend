import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCategories } from '../redux/actions/categoryActions';

const useRefreshCategories = () => {

    const dispatch = useDispatch();
    const {categories} = useSelector(state => state.categories);

    useEffect(() => {
        if(!categories){
            dispatch(getCategories());
        }
    }) 
}

export default useRefreshCategories;
