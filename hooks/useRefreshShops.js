import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getShops } from '../redux/actions/shopActions';

const useRefreshShops = () => {

    const dispatch = useDispatch();
    const {shops} = useSelector(state => state.shops);

    useEffect(() => {
        if(!shops){
            dispatch(getShops());
        }
    }) 
}

export default useRefreshShops;
