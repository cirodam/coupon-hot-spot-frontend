import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getFeatured, getLatest, getPopular, getSuggested } from '../redux/actions/dealActions';

const useRefreshDeals = () => {

    const dispatch = useDispatch();
    const {latest} = useSelector(state => state.deals);

    useEffect(() => {
        if(!latest){
            dispatch(getLatest());
            dispatch(getFeatured());
            dispatch(getSuggested());
            dispatch(getPopular());
        }
    }) 
}

export default useRefreshDeals;
