import React from 'react';
import styles from './ShopItem.module.css';
import Link from 'next/link'

const ShopItem = ({shop}) => {

    //Dont show anything if this store has no coupons
    if(shop.couponCount < 1){
        return null;
    }

    return (
        <div id={styles.shopItem}>
            { shop.imgLink ?
                <img src={`https://coupon-hotspot-storemedia.s3.amazonaws.com/${shop.imgLink}`} id={styles.shopImg} />
              :
                <h4 id={styles.shopName}>{shop.storeName}</h4>
            }
            <Link href={`shops/${shop.storeID}`}>            
                <div id={styles.shopAlt}>
                    <p id={styles.countText}>{shop.couponCount} deals</p>
                </div>
            </Link>
        </div>
    )
}

export default ShopItem
