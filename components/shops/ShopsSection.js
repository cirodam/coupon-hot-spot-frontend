import React from 'react';
import styles from './ShopsSection.module.css';
import { useSelector } from 'react-redux';
import ShopItem from './ShopItem';

const ShopsSection = () => {

    const {shops} = useSelector(state => state.shops);

    return (
        <div id={styles.shopSection}>
            <h2 id={styles.shopHeader}>Our Partners</h2>
            <div className={styles.divider}></div>
            <ul id={styles.shopList}>
                { 
                    shops && shops.map(s => (
                        <ShopItem key={s.shopName} shop={s}/>
                    ))
                }
            </ul>
        </div>
    )
}

export default ShopsSection
