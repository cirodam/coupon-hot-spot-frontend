import React from 'react';
import styles from './CouponModal.module.css';
import Link from 'next/link';

const CouponModal = ({show, onClose, coupon}) => {

    if(show === false) {
        return null;
    }

    const onCopyBtn = () => {

    }

    return (
        <>
            <div id={styles.modal}>
                <button id={styles.exitBtn} onClick={() => onClose()}><i className="fas fa-times"></i></button>
                <div id={styles.modalHeader}>
                    <h1 id={styles.title}>Coupon Details</h1>
                </div>
                <div id={styles.modalBody}>
                    <h2 id={styles.couponTitle}>{coupon.couponName}</h2>
                    <p id={styles.couponDesc}>{coupon.couponDescription}</p>
                    <p id={styles.catName}>{coupon.categoryName}</p>
                    {   coupon.couponCode ?
                        <div id={styles.code}>
                            <p id={styles.codeText}>{coupon.couponCode}</p>
                            <button id={styles.copyBtn} onClick={() => onCopyBtn()}><i className="fas fa-copy"></i></button>
                        </div> :
                        <p>This coupon doesn't require a code. Just click the link below!</p>
                    }
                    <a href={coupon.couponLink} target="_blank" rel="noopener noreferrer"><button id={styles.siteBtn}>Go To Site</button></a>
                </div>
            </div>
            <div id={styles.overlay}></div>
        </>
    )
}

export default CouponModal
