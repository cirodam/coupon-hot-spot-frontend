import React from 'react';
import styles from './Footer.module.css';
import Link from 'next/link';

const Footer = () => {
    return (
        <footer id={styles.footer}>
            <div id={styles.container}>
                <div className={styles.footerColumn}>
                    <h1 id={styles.columnHeader}>Quick Links</h1>
                    <ul>
                        <Link href="/"><li className={styles.navLink}>Home</li></Link>
                        <Link href="/categories"><li className={styles.navLink}>Categories</li></Link>
                        <Link href="/shops"><li className={styles.navLink}>Shops</li></Link>
                        <Link href="/contact"><li className={styles.navLink}>Contact Us</li></Link>
                    </ul>
                </div>
                <div className={styles.footerColumn}>
                    <h1 id={styles.columnHeader}>About Us</h1>
                    <p id={styles.about}>Coupon Hotspot is a website that focus on grabbing the best deals & coupons from all over the internet and even from offline stores so you can enjoy a discounted shopping each time you visit us.</p>
                </div>
            </div>
            <div id={styles.copyContainer}>
                <p id={styles.copyright}>Copyright © 2021 Arc Tetra Software Ltd. Co. All Rights Reserved</p>
            </div>
        </footer>
    )
}

export default Footer
