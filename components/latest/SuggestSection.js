import React from 'react';
import {useSelector } from 'react-redux';
import styles from './SuggestSection.module.css';
import PopularItem from './PopularItem';

const SuggestSection = () => {

    const {suggested} = useSelector(state => state.deals);

    return (
        <div id={styles.suggestSection}>
            <h4 id={styles.suggestHeader}><span id={styles.headerHighlight}>Top 5</span> Suggestions</h4>
            <ul>
                {
                    suggested && suggested.map(s => (
                        <PopularItem key={s.addTime} coupon={s}/>
                    ))
                }
            </ul>
        </div>
    )
}

export default SuggestSection
