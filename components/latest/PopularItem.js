import React from 'react';
import styles from "./PopularItem.module.css";
import { getCategoryImg } from '../../util';

const PopularItem = ({coupon}) => {
    return (
        <li className={styles.popItem}>
            <div id={styles.popImgContainer}>
                <img id={styles.popImg} src={coupon.imgLink ? coupon.imgLink : getCategoryImg(coupon.categoryName)} alt="" />
            </div>
            <div id={styles.popContent}>
                <h4 id={styles.popTitle}>{coupon.couponName}</h4>
                <p id={styles.popText}>{coupon.couponDescription}</p>
            </div>
        </li>
    )
}

export default PopularItem;
