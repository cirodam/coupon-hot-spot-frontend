import React from 'react';
import styles from './LatestSection.module.css';
import { useSelector } from 'react-redux';
import LatestItem from './LatestItem';
import TechSection from './TechSection';
import PopularSection from './PopularSection';
import SuggestSection from './SuggestSection';

const LatestSection = () => {

    let { latest } = useSelector(state => state.deals);
    latest = latest ? latest.slice(0,14) : null

    return (
        <div id={styles.latestSection}>
            <div id={styles.latestContent}>
                <div id={styles.latestHeader}>
                    <h2 id={styles.latestTitle}>Latest Offers</h2>
                </div>
                <div className={styles.divider}>
                    <div id={styles.left}></div>
                    <div id={styles.right}></div>
                </div>
                <ul id={styles.latestList}>
                    { latest &&
                        latest.map(l => (
                            <LatestItem key={l.addTime} coupon={l} />
                        )) 
                    }
                </ul>
            </div>
            <div id={styles.latestSidebar}>
                <TechSection />
                <PopularSection />
                <SuggestSection />
            </div>
        </div>
    )
}

export default LatestSection
