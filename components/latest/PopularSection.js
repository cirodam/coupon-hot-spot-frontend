import React from 'react';
import { useSelector } from 'react-redux';
import PopularItem from './PopularItem';
import styles from './PopularSection.module.css';

const PopularSection = () => {

    const {popular} = useSelector(state => state.deals);

    return (
        <div id={styles.popSection}>
            <h4 id={styles.techHeader}>Popular Savings!</h4>
            <div className={styles.divider}>
                <div id={styles.left}></div>
                <div id={styles.right}></div>
            </div>
            <ul>
                {
                    popular && popular.map(p => (
                        <PopularItem key={p.addTime} coupon={p}/>
                    ))
                }
            </ul>
        </div>
    )
}

export default PopularSection
