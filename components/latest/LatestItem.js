import React, {useState} from 'react';
import styles from './LatestItem.module.css';
import CouponModal from '../CouponModal';
import { getCategoryImg, getCreatedString, getExpiresString } from '../../util';

const LatestItem = ({coupon}) => {

    const [showModal, setShowModal] = useState(false);

    if(coupon.expireTime <= Date.now()/1000){ //Dont show expired coupons
        return null;
    }

    const onCouponClick = () => {
        if(window.innerWidth < 760) {
            setShowModal(true);
        }
    }

    return (
        <>
            <li className={styles.latestItem} onClick={() => onCouponClick()}>
                <div id={styles.imgContainer}>
                    <img src={coupon.imgLink ? coupon.imgLink : getCategoryImg(coupon.categoryName)} alt="img" id={styles.img}/>
                </div>
                <div id={styles.contentContainer}>
                    <div id={styles.content}>
                        <div id={styles.timeContent}>
                            <span className={styles.timeField}><i className="far fa-clock"></i> {getCreatedString(coupon.createTime)}</span>
                            <span className={styles.timeField}><i className="fas fa-hourglass" id={styles.hourglass}></i>{getExpiresString(coupon.expireTime)}</span>
                        </div>
                        <div>
                            <h4 id={styles.couponName}>{coupon.couponName}</h4>
                            <p id={styles.couponDesc}>{coupon.couponDescription}</p>
                        </div>
                        <p id={styles.shop}>{coupon.shopName}</p>
                    </div>
                    <button id={styles.codeBtn} onClick={() => setShowModal(true)}>{coupon.couponCode !== "" ? "Get Code" : "Show Deal"}</button>
                </div>
            </li>
            <CouponModal show={showModal} onClose={() => setShowModal(false)} coupon={coupon}/>
        </>
    )
}

export default LatestItem
