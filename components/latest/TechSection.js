import React from 'react';
import styles from './TechSection.module.css';
import Link from 'next/link';

const TechSection = () => {

    return (
        <section id={styles.techSection}>
            <h4 id={styles.techHeader}>Hot Tech Deals</h4>
            <div className={styles.divider}>
                <div id={styles.left}></div>
                <div id={styles.right}></div>
            </div>
            <Link href={`/techdeals`}><img src="techDeals.png" alt="" id={styles.techImg}/></Link>
        </section>
    )
}

export default TechSection
