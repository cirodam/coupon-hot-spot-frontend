import React, {useState} from 'react';
import styles from './CarouselItem.module.css';
import CouponModal from '../CouponModal';
import {getCategoryImg} from '../../util';

const CarouselItem = ({coupon}) => {

    const [showModal, setShowModal] = useState(false);

    if(coupon.expireTime <= Date.now()/1000){ //Dont show expired coupons
        return null;
    }

    return (
        <>
            <li className={styles.carouselItem} onClick={() => setShowModal(true)}>
                <div id={styles.imgOutline}>
                    <img src={coupon.imgLink ? coupon.imgLink : getCategoryImg(coupon.categoryName)} alt="" id={styles.carImg}/>
                </div>
                <div id={styles.content}>
                    <h3 id={styles.carTitle}>{coupon.couponName}</h3>
                    <p id={styles.carDesc}>{coupon.couponDescription}</p>
                    <p id={styles.carCat}>{coupon.categoryName}</p>
                </div>
            </li>
            <CouponModal show={showModal} onClose={() => setShowModal(false)} coupon={coupon}/>
        </>
    )
}

export default CarouselItem;
