import React, {useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import CarouselItem from './CarouselItem';
import styles from './CarouselSection.module.css';

const CarouselSection = () => {

    const {featured} = useSelector(state => state.deals);

    const [start, setStart] = useState(0);
    const [range, setRange] = useState(3);

    useEffect(() => {
        if(window){
            if(window.screen.width < 1600){
                setRange(2);
            }
            if(window.screen.width < 900) {
                setRange(1);
            }
        }
    })

    const onRightBtn = () => {
        if(start === featured.length-1){
            setStart(0);
        }else{
            setStart(start+1)
        }
    }

    const onLeftBtn = () => {
        if(start === 0){
            setStart(featured.length-1);
        }else{
            setStart(start-1)
        }
    }

    const getIndices = () => {
        let indices = [];
        for(let i=start; i<start+range; i++){
            if(i < featured.length){
                indices.push(featured[i]);
            } else {
                indices.push(featured[i-featured.length])
            }
        }
        return indices
    }

    return (
        <div id={styles.carouselSection}>
            {/*<button className={styles.toggleBtn} onClick={() => onLeftBtn()}><i className="fas fa-chevron-left"></i></button>*/}
            <ul id={styles.carouselList}>
                { featured &&
                    featured.map(c => (
                        <CarouselItem key={c.couponID} coupon={c}/>
                    ))
                }
            </ul>
            {/*<button className={styles.toggleBtn} onClick={() => onRightBtn()}><i className="fas fa-chevron-right"></i></button>*/}
        </div>
    )
}

export default CarouselSection;
