import React from 'react';
import styles from './HeroSection.module.css';

const HeroSection = () => {

    return (
        <section id={styles.heroSection}>
            <div id={styles.container}>
                <div id={styles.heroContent}>
                    <h1 id={styles.heroHeader}>We'll get you the best deal</h1>
                    <p id={styles.heroText}>Coupon Hotspot helps you shop smarter & save money. Find current hot deals and coupon codes from your favorite stores</p>
                    <div id={styles.buttonRow}>
                        <button className={styles.jumpButton}>Jump to Deals</button>
                        <button className={styles.transButton}>How it Works</button>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HeroSection;
