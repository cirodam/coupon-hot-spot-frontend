import React, {useState} from 'react';
import styles from './Navbar.module.css';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Navbar = () => {

    const router = useRouter();
    const [query, setQuery] = useState("");

    const onSearchBtn = () => {
        router.push(`/search?term=${query}&page=0`)
    }

    return (
        <nav id={styles.nav}>
            <div id={styles.navTop}>
                <div id={styles.container}>
                    <Link href="/"><img id={styles.navLogo} src="/logo.png" alt="Logo"/></Link>
                    <div id={styles.searchGroup}>
                        <input type="text" id={styles.searchInput} placeholder="Search" value={query} onChange={(e) => setQuery(e.target.value)}></input>
                        <i className="fas fa-search" id={styles.searchBtn} onClick={() => onSearchBtn()}></i>
                    </div>
                </div>
            </div>
            <div id={styles.navBottom}>
                <ul id={styles.navLinks}>
                    <Link href="/shops"><a className={styles.navLink}>Stores</a></Link>
                    <Link href="/categories"><a className={styles.navLink}>Categories</a></Link>
                    <Link href="/latest"><a className={styles.navLink}>Latest Deals</a></Link>
                </ul>
            </div>
        </nav>
    )
}

export default Navbar;
