import React from 'react';
import styles from './CategoryItem.module.css';
import Link from 'next/link'

const CategoryItem = ({category}) => {

    if(category.couponCount < 1){
        return null; 
    }

    return (
        <div id={styles.catItem}>
            <div id={styles.catImg}>
                <h6 id={styles.entryName}>{category.categoryName}</h6>
            </div>
            <Link href={`/categories/${encodeURIComponent(category.categoryName)}`}>            
                <div id={styles.catAlt}>
                    <p id={styles.countText}>{category.couponCount} deals</p>
                </div>
            </Link>
        </div>
    )
}

export default CategoryItem
