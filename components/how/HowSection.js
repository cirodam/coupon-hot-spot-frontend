import React from 'react';
import styles from './HowSection.module.css';

const HowSection = () => {
    return (
        <div id={styles.howSection}>
            <h4 id={styles.howHeader}>How to Use Our Coupons</h4>
            <div className={styles.divider}></div>
            <div id={styles.howContainer}>
                <div className={styles.howEntry}>
                    <h5 id={styles.howTitle}>Discount Codes</h5>
                    <p id={styles.howText}>When you click on any coupon code on our website you will then can copy the code and use it on the specified website chosen by you.</p>
                </div>
                <div className={styles.howEntry}>
                    <h5 id={styles.howTitle}>Discount Links</h5>
                    <p id={styles.howText}>In the case of a discount link it’s a different story, you will just need to click on the discount link and it will take you directly to the offer website so no need to use a coupon code.</p>
                </div>
                <div className={styles.howEntry}>
                    <h5 id={styles.howTitle}>Offers</h5>
                    <p id={styles.howText}>Here in Coupons Hotspot you will find daily deals from big stores like amazon, ebay, banggodd and a lot more just keep any eye on the website or just simply subscribe to our newsletter to get daily hot offers.</p>
                </div>
            </div>
        </div>
    )
}

export default HowSection
