import React, {useState} from 'react';
import styles from './SearchItem.module.css';
import CouponModal from '../components/CouponModal';
import {getCategoryImg, getExpiresString} from '../util';

const SearchItem = ({coupon}) => {

    const [showModal, setShowModal] = useState(false);

    if(coupon.expireTime < Date.now()/1000){ //Dont show expired coupons
        return null;
    }

    return (
        <>
            <li className={styles.searchEntry}>
                <div id={styles.imgContainer}>
                    <img src={coupon.imgLink ? coupon.imgLink : getCategoryImg(coupon.categoryName)} alt="" id={styles.searchImg}/>
                </div>
                <div id={styles.entryContent}>
                    <h3 id={styles.entryTitle}>{coupon.couponName}</h3>
                    <p id={styles.entryDesc}>{coupon.couponDescription}</p>
                    <button id={styles.useButton} onClick={() => setShowModal(true)}>Use Now</button>
                    <div id={styles.bottomRow}>
                        <p id={styles.site}>{coupon.shopName}</p>
                        <p id={styles.site}><i className="fas fa-hourglass" id={styles.hourglass}></i>{getExpiresString(coupon.expireTime)}</p>
                    </div>
                </div>
                <CouponModal show={showModal} onClose={() => setShowModal(false)} coupon={coupon}/>
            </li>
        </>
    )
}

export default SearchItem
