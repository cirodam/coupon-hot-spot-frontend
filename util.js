export const getOptions = () => {
    return {
        headers: {
            'Content-Type': 'application/json'
        }
    };
}

export const getCategoryImg = (name) => {
    switch(name){
        case "Health & Wellness":
            return "/categories/health_draw.jpg";
        
        case "Kitchen & Dining":
            return "/categories/kitchen_draw.jpg";

        case "Food & Groceries":
            return "/categories/food.jpg";

        case "Lawn & Garden":
            return "/categories/lawn_draw.jpg";

        case "Pets":
            return "/categories/pets_draw.jpg";

        case "Computers":
            return "/categories/computers.jpg";

        case "Cellphones":
            return "/categories/cellphone.jpg";

        case "Hobbies & Crafts":
            return "/categories/crafts_draw.jpg";

        case "Office Supplies":
            return "/categories/office_draw.jpg";

        case "Hiking & Camping":
            return "/categories/hiking.jpg";

        case "Jewelry":
            return "/categories/jewelry_draw.jpg";

        case "Home Audio":
            return "/categories/homeaudio.jpg";

        case "Home Improvement":
            return "/categories/home_draw.jpg";

        case "Clothing & Shoes":
            return "/categories/clothing_draw.jpg";

        case "Televisions":
            return "/categories/tv.jpg";

        case "Smart Home":
            return "/categories/smarthome.jpg";

        case "Gaming":
            return "/categories/gaming.jpg";

        case "Toys & Board Games":
            return "/categories/toys.jpg";

        case "Holiday":
            return "/categories/holiday.jpg";

        case "Travel":
            return "/categories/travel_draw.jpg";

        case "Education":
            return "/categories/education.jpg";

        case "Professional":
            return "/categories/professional.jpg";

        case "Bath & Beauty":
            return "/categories/bath_draw.jpg";

        case "Baby & Nursery":
            return "/categories/baby_draw.jpg";

        case "Major Appliances":
            return "/categories/fridge_draw.jpg";

        case "Children":
            return "/categories/kids_draw.jpg";

        case "Financial":
            return "/categories/finance.png";

        case "Sports":
            return "/categories/sports.jpg";

        case "Gifts":
            return "/categories/gift.jpg";

        case "Subscriptions":
            return "/categories/sub.jpg";

        case "Self Help":
            return "/categories/selfhelp.jpg";

        case "Tools":
            return "/categories/tools.jpg";

        case "Vision Care":
            return "/categories/vision_draw.jpg";

        case "Fitness":
            return "/categories/fitness.jpg";

        case "Home Decor":
            return "/categories/decor_draw.jpg";

        case "Automotive":
            return "/categories/auto.jpg";

        default:
            return "tech.jpg"
    }
}

export const getShopName = (id) => {
    switch(id){
        case 1:
            return "Amazon.com";
        case 2:
            return "Ebay.com";
        default:
            return "Unknown"
    }
}

export const getCreatedString = (createTime) => {

    const currentTime = Math.round((new Date()).getTime()/1000)
    const elapsed = currentTime - createTime;

    const minutes = elapsed/60;
    const hours = minutes/60;

    if(minutes < 60){
        return Math.floor(minutes) + " minutes ago";
    }

    if(hours <= 24){
        return Math.floor(hours) + " hours ago";
    } else if (elapsed > 24 && elapsed <= 8760){
        return Math.floor(hours/24) + " days ago";
    }
    return "1 Year ago";
}

export const getExpiresString = (expireTime) => {

    const currentTime = Math.round((new Date()).getTime()/1000)
    const remaining = Math.floor((expireTime - currentTime)/60/60);

    if(remaining <= 24){
        return remaining + " hours left";   
    } else if (remaining > 24 && remaining <= 8760){
        return Math.round(remaining/24) + " days left";
    }

    return "No Expiration";
}